module.exports = {
  extends: ['@react-native-community', 'plugin:@typescript-eslint/recommended', 'plugin:prettier/recommended'],
  parser: '@typescript-eslint/parser',
  plugins: ['@typescript-eslint', 'prettier'],
  root: true,
  rules: {
    '@typescript-eslint/explicit-module-boundary-types': 'off', // Checks implicit return type.
  },
}
