import AsyncStorage from '@react-native-async-storage/async-storage'
import { AtomEffect } from 'recoil'

export function asyncStorageEffect<State>(key: string): AtomEffect<State> {
  return ({ setSelf, onSet }) => {
    // 저장된 값이 있다면 설정한다
    loadValue<State>(key).then((value) => value && setSelf(value))
    onSet((newValue) => {
      if (!newValue) {
        return AsyncStorage.removeItem(key)
      }
      AsyncStorage.setItem(key, JSON.stringify(newValue))
    })
  }
}

export async function loadValue<State = string>(key: string): Promise<State | null> {
  const saved = await AsyncStorage.getItem(key)
  if (saved === null) return null
  return JSON.parse(saved)
}
