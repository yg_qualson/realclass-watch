import axios, { AxiosRequestConfig, AxiosError } from 'axios'

import { ACCESS_TOKEN } from './auth/atoms'
import { loadValue } from './storage'

const serviceEndpoints: { [k: string]: string } = {
  dev: 'https://api-v2-dev.realclass.co.kr',
  stage: 'https://api-v2-stage.realclass.co.kr',
  live: 'https://api-v2.realclass.co.kr',
}

const DEPLOY_TYPE = process.env.DEPLOY_TYPE || 'dev'
const API_URL = (serviceEndpoints[DEPLOY_TYPE] || 'https://api-v2-dev.realclass.co.kr') + '/v2/api'

const axiosConfig = {
  withCredentials: true,
  timeout: DEPLOY_TYPE === 'dev' ? 60000 : 10000,
  headers: {
    'X-Requested-With': 'XMLHttpRequest',
    'Content-Type': 'application/json',
  },
}

export const PublicAPI = axios.create({
  baseURL: `${API_URL}`,
  ...axiosConfig,
})

export const PrivateAPI = axios.create({
  baseURL: `${API_URL}/profiles/me`,
  ...axiosConfig,
})

async function useToken(config: AxiosRequestConfig): Promise<AxiosRequestConfig> {
  const token = await loadValue<string>(ACCESS_TOKEN)
  if (token) {
    // eslint-disable-next-line @typescript-eslint/no-unsafe-member-access
    config.headers = config.headers || {}
    config.headers.Authorization = `Bearer ${token}`
  }
  return config
}

PublicAPI.interceptors.request.use(useToken)
PrivateAPI.interceptors.request.use(useToken)

export function isAxiosError(error: unknown): error is AxiosError {
  return (error as AxiosError)?.isAxiosError
}
