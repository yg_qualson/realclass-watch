import { atom } from 'recoil'

import { asyncStorageEffect } from '../storage'

export const ACCESS_TOKEN = 'AUTH/ACCESS_TOKEN'

export const accessToken = atom<string | null>({
  key: 'auth/access-token',
  default: null,
  effects_UNSTABLE: [asyncStorageEffect(ACCESS_TOKEN)],
})
