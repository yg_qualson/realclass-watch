import { CourseSummary, Media, MediaSummary } from './models'

export interface FetchMyCoursesResponse {
  result: CourseSummary[]
}

export interface FetchMediaListPayload {
  courseId: number
}

export interface FetchMediaListResponse {
  result: MediaSummary[]
}

export interface FetchMediaPayload {
  seasonId: number
  mediaId: number
}

export interface FetchMediaResponse {
  result: Media
}
