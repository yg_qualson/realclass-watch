import { PublicAPI, PrivateAPI } from '../ajax'
import * as T from './types'

export const fetchMyCourses = () => PrivateAPI.get<T.FetchMyCoursesResponse>('/courses')
export const fetchMediaList = ({ courseId }: T.FetchMediaListPayload) =>
  PublicAPI.get<T.FetchMediaListResponse>(`/courses/${courseId}/medias`)
export const fetchMedia = ({ seasonId, mediaId }: T.FetchMediaPayload) =>
  PublicAPI.get<T.FetchMediaResponse>(`/seasons/${seasonId}/medias/${mediaId}`)
