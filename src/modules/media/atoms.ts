import { selector, selectorFamily } from 'recoil'

import { COURSE_TO_SEASON, SEASONS } from '@/data/seasons'

import * as API from './apis'

export const mySeasons = selector({
  key: 'media/mySeasons',
  get: async () => {
    const response = await API.fetchMyCourses()
    const courseSummaries = response.data.result
    const seasonIds = courseSummaries.map((course) => {
      return {
        courseId: course.id,
        seasonId: COURSE_TO_SEASON[course.id],
      }
    })

    const seasonMap: { [key: number]: number[] } = {}
    seasonIds.forEach(({ courseId, seasonId }) => {
      if (!seasonId) return
      seasonMap[seasonId] = seasonMap[seasonId] || []
      seasonMap[seasonId].push(courseId)
    })
    const seasons = Object.entries(seasonMap).map(([seasonId, courseIds]) => ({
      seasonId,
      courseIds,
      season: SEASONS[seasonId],
    }))
    return seasons
  },
})

export const mediaList = selectorFamily({
  key: 'media/mediaList',
  get: (courseId: number) => async () => {
    const response = await API.fetchMediaList({ courseId })
    return response.data.result
  },
})

export const media = selectorFamily({
  key: 'media/media',
  get:
    ({ seasonId, mediaId }: { seasonId: number; mediaId: number }) =>
    async () => {
      const response = await API.fetchMedia({ seasonId, mediaId })
      return response.data.result
    },
})
