export interface CourseSummary {
  id: number
  name: string
  type: string
  order: number
  categoryId: number
  subCategoryId: number | null
  seasonIndex: number
  selectorName: string
}

export interface MediaSummary {
  id: number
  startTime: number
  endTime: number
  name: string
  thumbnailUrl: string
}

export interface Media extends MediaSummary {
  subtitles: Subtitle[]
  videoUrlFairplay: string
  videoUrlWidevine: string
}

export interface Subtitle {
  id: number
  startTime: number
  endTime: number
  textEng: string
}
