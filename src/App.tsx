import { NavigationContainer } from '@react-navigation/native'
import React, { useEffect } from 'react'
import Orientation from 'react-native-orientation'
import { RecoilRoot, useRecoilValue } from 'recoil'

import { accessToken } from './modules/auth/atoms'
import GuestNavigation from './navigation/GuestNavigation'
import UserNavigation from './navigation/UserNavigation'

const App = () => {
  const token = useRecoilValue(accessToken)

  // lock to portrait, as default
  useEffect(() => Orientation.lockToPortrait(), [])

  return (
    <NavigationContainer>
      {token && <UserNavigation />}
      {!token && <GuestNavigation />}
    </NavigationContainer>
  )
}

export default () => (
  <RecoilRoot>
    <App />
  </RecoilRoot>
)
