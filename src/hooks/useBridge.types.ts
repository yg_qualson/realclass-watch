interface WebviewMessageDataBase {
  type: string
  payload?: unknown
}

interface SetTokenData extends WebviewMessageDataBase {
  type: 'setToken'
  payload: {
    accessToken: string
    refreshToken: string
  }
}
interface ClearTokenData extends WebviewMessageDataBase {
  type: 'clearToken'
}

export type WebviewMessageData = SetTokenData | ClearTokenData
