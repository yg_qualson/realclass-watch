import { useCallback } from 'react'
import { WebViewMessageEvent } from 'react-native-webview'
import { useSetRecoilState } from 'recoil'

import { accessToken } from '@/modules/auth/atoms'

import { WebviewMessageData } from './useBridge.types'

const useBridge = () => {
  const setAccessToken = useSetRecoilState(accessToken)
  const messageHandler = useCallback(
    (event: WebViewMessageEvent) => {
      const data = JSON.parse(event.nativeEvent.data) as WebviewMessageData
      console.log('BRIDGE: ', data.type, data.payload)

      switch (data.type) {
        case 'setToken': {
          return setAccessToken(data.payload.accessToken)
        }
        case 'clearToken': {
          return setAccessToken(null)
        }

        default:
          console.log(data)
          return
      }
    },
    [setAccessToken]
  )

  return messageHandler
}

export default useBridge
