export const LECTURE_LINKED_SUBTITLES = [
  {
    clipId: 4488,
    media: 1945,
    subtitles: [
      {
        id: 250852,
        lectures: [
          {
            id: '99-6635-9791',
            phrase: 'cut it out!',
            thumbnailUrl:
              'https://public.realclass.co.kr/video/lecture/cf5c752c8b8d4a7a9712f152624dd10b/cf5c752c8b8d4a7a9712f152624dd10b.jpg',
          },
        ],
      },
      {
        id: 250916,
        lectures: [
          {
            id: '99-6635-9792',
            phrase: 'I hate it when he does that.',
            thumbnailUrl:
              'https://public.realclass.co.kr/video/lecture/865c298fbeed414092603e7addeda4f1/865c298fbeed414092603e7addeda4f1.jpg',
          },
        ],
      },
      {
        id: 250922,
        lectures: [
          {
            id: '99-6635-9793',
            phrase: 'Forget it.',
            thumbnailUrl:
              'https://public.realclass.co.kr/video/lecture/54016e7f210b41a1adbbfd710cfc3bbd/54016e7f210b41a1adbbfd710cfc3bbd.jpg',
          },
        ],
      },
    ],
  },
  {
    clipId: 4489,
    media: 1946,
    subtitles: [
      {
        id: 250949,
        lectures: [
          {
            id: '99-6636-9794',
            phrase: 'Movies in the classroom can be one of our best learning tools.',
            thumbnailUrl:
              'https://public.realclass.co.kr/video/lecture/8b158120be534036b9f63033cfb1ad68/8b158120be534036b9f63033cfb1ad68.jpg',
          },
        ],
      },
      {
        id: 250973,
        lectures: [
          {
            id: '99-6636-9795',
            phrase: 'sometimes you just have to open the book',
            thumbnailUrl:
              'https://public.realclass.co.kr/video/lecture/ee3486dfefc0432cbbb2d085917eb0f2/ee3486dfefc0432cbbb2d085917eb0f2.jpg',
          },
        ],
      },
    ],
  },
  {
    clipId: 4490,
    media: 1947,
    subtitles: [
      {
        id: 251026,
        lectures: [
          {
            id: '99-6637-9796',
            phrase: 'It’s nothing to worry about.',
            thumbnailUrl:
              'https://public.realclass.co.kr/video/lecture/bce68fb4e8974bbf9cddba10c029d993/bce68fb4e8974bbf9cddba10c029d993.jpg',
          },
        ],
      },
      {
        id: 251037,
        lectures: [
          {
            id: '99-6637-9797',
            phrase: "I'll see what I can do.",
            thumbnailUrl:
              'https://public.realclass.co.kr/video/lecture/4852203dae3e4721992ef955e56ed2a4/4852203dae3e4721992ef955e56ed2a4.jpg',
          },
        ],
      },
    ],
  },
  {
    clipId: 4491,
    media: 1948,
    subtitles: [
      {
        id: 251088,
        lectures: [
          {
            id: '99-6638-9798',
            phrase: "That's close enough!",
            thumbnailUrl:
              'https://public.realclass.co.kr/video/lecture/45a91f1ae43240edb82edff00c877bf2/45a91f1ae43240edb82edff00c877bf2.jpg',
          },
        ],
      },
    ],
  },
  {
    clipId: 4492,
    media: 1949,
    subtitles: [
      {
        id: 251161,
        lectures: [
          {
            id: '99-6639-9799',
            phrase: 'What if my hand gets caught in the mail slot?',
            thumbnailUrl:
              'https://public.realclass.co.kr/video/lecture/7026af5359444edfb3104f7c2b5858b3/7026af5359444edfb3104f7c2b5858b3.jpg',
          },
        ],
      },
      {
        id: 251168,
        lectures: [
          {
            id: '99-6639-9800',
            phrase: 'Do me a favor.',
            thumbnailUrl:
              'https://public.realclass.co.kr/video/lecture/e320dacfc7954afe8d19da036eeb3dec/e320dacfc7954afe8d19da036eeb3dec.jpg',
          },
        ],
      },
    ],
  },
  {
    clipId: 4493,
    media: 1950,
    subtitles: [
      {
        id: 251262,
        lectures: [
          {
            id: '99-6640-9801',
            phrase: 'What’s wrong with you?',
            thumbnailUrl:
              'https://public.realclass.co.kr/video/lecture/b30bffac2d27454c9db545b6a065bc9b/b30bffac2d27454c9db545b6a065bc9b.jpg',
          },
        ],
      },
      {
        id: 251269,
        lectures: [
          {
            id: '99-6640-9802',
            phrase: 'That’s 49 fly balls in a row!',
            thumbnailUrl:
              'https://public.realclass.co.kr/video/lecture/974302e212d749e78d0ee966792d8a53/974302e212d749e78d0ee966792d8a53.jpg',
          },
        ],
      },
      {
        id: 251302,
        lectures: [
          {
            id: '99-6640-9803',
            phrase: 'I’ve always wanted to learn how to play football.',
            thumbnailUrl:
              'https://public.realclass.co.kr/video/lecture/8b06d0f70f04442fb87ceb1fe7e37882/8b06d0f70f04442fb87ceb1fe7e37882.jpg',
          },
        ],
      },
    ],
  },
  {
    clipId: 4494,
    media: 1951,
    subtitles: [
      {
        id: 251335,
        lectures: [
          {
            id: '99-6641-9804',
            phrase: 'How can I practice piano with a stupid bird bothering me all the time?!',
            thumbnailUrl:
              'https://public.realclass.co.kr/video/lecture/bda337c7d0fe480ca5b33ecf61501cb6/bda337c7d0fe480ca5b33ecf61501cb6.jpg',
          },
        ],
      },
      {
        id: 251373,
        lectures: [
          {
            id: '99-6641-9805',
            phrase: 'Nobody could be that happy!',
            thumbnailUrl:
              'https://public.realclass.co.kr/video/lecture/a9fbc4016109422cb2f6347a00c04b03/a9fbc4016109422cb2f6347a00c04b03.jpg',
          },
        ],
      },
    ],
  },
  {
    clipId: 4495,
    media: 1952,
    subtitles: [
      {
        id: 251437,
        lectures: [
          {
            id: '99-6642-9806',
            phrase: 'On second thought, that may be my locker combination.',
            thumbnailUrl:
              'https://public.realclass.co.kr/video/lecture/202302b42c65486d851ec0fdce31e633/202302b42c65486d851ec0fdce31e633.jpg',
          },
        ],
      },
      {
        id: 251441,
        lectures: [
          {
            id: '99-6642-9807',
            phrase: 'Guess what, big brother?',
            thumbnailUrl:
              'https://public.realclass.co.kr/video/lecture/fa6dabc29b9942cc9842901e93e42a6e/fa6dabc29b9942cc9842901e93e42a6e.jpg',
          },
        ],
      },
      {
        id: 251490,
        lectures: [
          {
            id: '99-6642-9808',
            phrase: 'Don’t count on it.',
            thumbnailUrl:
              'https://public.realclass.co.kr/video/lecture/edd4593e909c437bbc5bbc7d12d906da/edd4593e909c437bbc5bbc7d12d906da.jpg',
          },
        ],
      },
    ],
  },
  {
    clipId: 4496,
    media: 1953,
    subtitles: [
      {
        id: 251519,
        lectures: [
          {
            id: '99-6643-9809',
            phrase: 'But it doesn’t necessarily have to.',
            thumbnailUrl:
              'https://public.realclass.co.kr/video/lecture/0cb9a99fdf71415186886c2b02437fd8/0cb9a99fdf71415186886c2b02437fd8.jpg',
          },
        ],
      },
      {
        id: 251559,
        lectures: [
          {
            id: '99-6643-9810',
            phrase: "We'll see!",
            thumbnailUrl:
              'https://public.realclass.co.kr/video/lecture/0b0cf19f7197439b938887af835de09c/0b0cf19f7197439b938887af835de09c.jpg',
          },
        ],
      },
    ],
  },
  {
    clipId: 4497,
    media: 1954,
    subtitles: [
      {
        id: 251586,
        lectures: [
          {
            id: '99-6644-9811',
            phrase: "I'm taking this leaf to school for show and tell.",
            thumbnailUrl:
              'https://public.realclass.co.kr/video/lecture/0588bab62af447bf8ff42477a5e2f633/0588bab62af447bf8ff42477a5e2f633.jpg',
          },
        ],
      },
      {
        id: 251588,
        lectures: [
          {
            id: '99-6644-9812',
            phrase: 'we all feel sort of sad when the leaves begin to fall.',
            thumbnailUrl:
              'https://public.realclass.co.kr/video/lecture/1f5fcf9f74ef4f1e9f5b9c6ce5d1ea31/1f5fcf9f74ef4f1e9f5b9c6ce5d1ea31.jpg',
          },
        ],
      },
      {
        id: 251631,
        lectures: [
          {
            id: '99-6644-9813',
            phrase: 'Stick to it.',
            thumbnailUrl:
              'https://public.realclass.co.kr/video/lecture/7c2fa97661e04cfdaf12c326687f4a9a/7c2fa97661e04cfdaf12c326687f4a9a.jpg',
          },
        ],
      },
    ],
  },
  {
    clipId: 4498,
    media: 1955,
    subtitles: [
      {
        id: 251697,
        lectures: [
          {
            id: '99-6645-9814',
            phrase: 'Why should I?',
            thumbnailUrl:
              'https://public.realclass.co.kr/video/lecture/c78d73a1350d4d46a4b8a184512fc452/c78d73a1350d4d46a4b8a184512fc452.jpg',
          },
        ],
      },
      {
        id: 251700,
        lectures: [
          {
            id: '99-6645-9815',
            phrase: 'they won’t let me bring any little kitty cats in.',
            thumbnailUrl:
              'https://public.realclass.co.kr/video/lecture/47bc981a016b435a84829b2d508d0b69/47bc981a016b435a84829b2d508d0b69.jpg',
          },
        ],
      },
    ],
  },
  {
    clipId: 4499,
    media: 1956,
    subtitles: [
      {
        id: 251770,
        lectures: [
          {
            id: '99-6646-9816',
            phrase: 'What makes you guys think you deserve a special week?',
            thumbnailUrl:
              'https://public.realclass.co.kr/video/lecture/0fb27885883549d495dbaa7544ffabc4/0fb27885883549d495dbaa7544ffabc4.jpg',
          },
        ],
      },
      {
        id: 251791,
        lectures: [
          {
            id: '99-6646-9817',
            phrase: 'I guess it’s no use.',
            thumbnailUrl:
              'https://public.realclass.co.kr/video/lecture/f026a9c0794140c9a2dca3a4d0164b44/f026a9c0794140c9a2dca3a4d0164b44.jpg',
          },
        ],
      },
      {
        id: 251800,
        lectures: [
          {
            id: '99-6646-9818',
            phrase: 'I’ll give you a second chance.',
            thumbnailUrl:
              'https://public.realclass.co.kr/video/lecture/e99fe313b8734e11baac0430b4a56ee9/e99fe313b8734e11baac0430b4a56ee9.jpg',
          },
        ],
      },
    ],
  },
  {
    clipId: 4500,
    media: 1957,
    subtitles: [
      {
        id: 251849,
        lectures: [
          {
            id: '99-6647-9819',
            phrase: 'Why take it out on me?',
            thumbnailUrl:
              'https://public.realclass.co.kr/video/lecture/f2e13a169cc445ed8c34f9b182228a52/f2e13a169cc445ed8c34f9b182228a52.jpg',
          },
        ],
      },
      {
        id: 251862,
        lectures: [
          {
            id: '99-6647-9820',
            phrase: 'All right, Charlie Brown, let’s put it another way.',
            thumbnailUrl:
              'https://public.realclass.co.kr/video/lecture/972e414fe94042fda3af710ee4ec97b6/972e414fe94042fda3af710ee4ec97b6.jpg',
          },
        ],
      },
    ],
  },
  {
    clipId: 4501,
    media: 1958,
    subtitles: [
      {
        id: 251903,
        lectures: [
          {
            id: '99-6648-9821',
            phrase: 'I mean, do you know what I’m talking about?',
            thumbnailUrl:
              'https://public.realclass.co.kr/video/lecture/2d53ce6dd78147cdb6585dd4255452a8/2d53ce6dd78147cdb6585dd4255452a8.jpg',
          },
        ],
      },
      {
        id: 251933,
        lectures: [
          {
            id: '99-6648-9822',
            phrase: 'What’s that got to do with love?',
            thumbnailUrl:
              'https://public.realclass.co.kr/video/lecture/815368971f46486396708981454346d4/815368971f46486396708981454346d4.jpg',
          },
        ],
      },
    ],
  },
  {
    clipId: 4502,
    media: 1959,
    subtitles: [
      {
        id: 252001,
        lectures: [
          {
            id: '99-6649-9823',
            phrase: 'I feel like going home to bed, but it’s only noon.',
            thumbnailUrl:
              'https://public.realclass.co.kr/video/lecture/b3f42fde8f62442b85dbaf98e0c9a149/b3f42fde8f62442b85dbaf98e0c9a149.jpg',
          },
        ],
      },
      {
        id: 252015,
        lectures: [
          {
            id: '99-6649-9824',
            phrase: 'I thought we were gonna take turns using that umbrella?',
            thumbnailUrl:
              'https://public.realclass.co.kr/video/lecture/b79a63378ae14cf19ebc6e0569a02f19/b79a63378ae14cf19ebc6e0569a02f19.jpg',
          },
        ],
      },
    ],
  },
  {
    clipId: 4503,
    media: 1960,
    subtitles: [
      {
        id: 252057,
        lectures: [
          {
            id: '99-6650-9825',
            phrase: 'No way!',
            thumbnailUrl:
              'https://public.realclass.co.kr/video/lecture/76418fc1b63041028c68ae24c4f0a892/76418fc1b63041028c68ae24c4f0a892.jpg',
          },
        ],
      },
      {
        id: 252091,
        lectures: [
          {
            id: '99-6650-9826',
            phrase: 'I can’t write with your elbow in my way, Chuck.',
            thumbnailUrl:
              'https://public.realclass.co.kr/video/lecture/c7655026726e40eea978969fea136347/c7655026726e40eea978969fea136347.jpg',
          },
        ],
      },
    ],
  },
  {
    clipId: 4504,
    media: 1961,
    subtitles: [
      {
        id: 252147,
        lectures: [
          {
            id: '99-6651-9827',
            phrase: 'Everyone, stay in position and keep your eye on the ball!',
            thumbnailUrl:
              'https://public.realclass.co.kr/video/lecture/a31eee13ee3d4a01882b6fdc7422c1ce/a31eee13ee3d4a01882b6fdc7422c1ce.jpg',
          },
        ],
      },
      {
        id: 252174,
        lectures: [
          {
            id: '99-6651-9828',
            phrase: 'They left out "Pay attention."',
            thumbnailUrl:
              'https://public.realclass.co.kr/video/lecture/34a4ad0a0a344d7f9403047f9b9251d5/34a4ad0a0a344d7f9403047f9b9251d5.jpg',
          },
        ],
      },
    ],
  },
  {
    clipId: 4505,
    media: 1962,
    subtitles: [
      {
        id: 252233,
        lectures: [
          {
            id: '99-6652-9829',
            phrase: 'it’s about time she started to raise a family of her own.',
            thumbnailUrl:
              'https://public.realclass.co.kr/video/lecture/586aabe0e8f74d7d99e4afe6311ff78d/586aabe0e8f74d7d99e4afe6311ff78d.jpg',
          },
        ],
      },
      {
        id: 252258,
        lectures: [
          {
            id: '99-6652-9830',
            phrase: 'Oh, miss Othmar, how could you?',
            thumbnailUrl:
              'https://public.realclass.co.kr/video/lecture/2846c042b54e4f91b269a656d93d4f86/2846c042b54e4f91b269a656d93d4f86.jpg',
          },
        ],
      },
    ],
  },
  {
    clipId: 4506,
    media: 1963,
    subtitles: [
      {
        id: 252306,
        lectures: [
          {
            id: '99-6653-9831',
            phrase: 'you’re in for some serious trouble, Snoopy.',
            thumbnailUrl:
              'https://public.realclass.co.kr/video/lecture/2d935cb0ef5e4d0fbd0ee35cf3aa8716/2d935cb0ef5e4d0fbd0ee35cf3aa8716.jpg',
          },
        ],
      },
      {
        id: 252332,
        lectures: [
          {
            id: '99-6653-9832',
            phrase: 'I believe in you!',
            thumbnailUrl:
              'https://public.realclass.co.kr/video/lecture/f068b06b58c546e78c6b38011a3cf26e/f068b06b58c546e78c6b38011a3cf26e.jpg',
          },
        ],
      },
    ],
  },
  {
    clipId: 4507,
    media: 1964,
    subtitles: [
      {
        id: 252384,
        lectures: [
          {
            id: '99-6654-9833',
            phrase: 'Okay, mister “No shoes,“ what’s so funny?',
            thumbnailUrl:
              'https://public.realclass.co.kr/video/lecture/1b9a13065a5e4917b5ca1c44945ea77e/1b9a13065a5e4917b5ca1c44945ea77e.jpg',
          },
        ],
      },
      {
        id: 252404,
        lectures: [
          {
            id: '99-6654-9834',
            phrase: 'I’ll never make it.',
            thumbnailUrl:
              'https://public.realclass.co.kr/video/lecture/a12d3b76d39a403bb740eee982a54615/a12d3b76d39a403bb740eee982a54615.jpg',
          },
        ],
      },
    ],
  },
  {
    clipId: 4508,
    media: 1965,
    subtitles: [
      {
        id: 252437,
        lectures: [
          {
            id: '99-6655-9835',
            phrase: 'Can’t you write about something nice?',
            thumbnailUrl:
              'https://public.realclass.co.kr/video/lecture/067bc7f78afb424684216fcfb8144394/067bc7f78afb424684216fcfb8144394.jpg',
          },
        ],
      },
      {
        id: 252481,
        lectures: [
          {
            id: '99-6655-9836',
            phrase: "No, ma'am, we’re not making fun of your class.",
            thumbnailUrl:
              'https://public.realclass.co.kr/video/lecture/40432478054d42deb49876d20376c0f6/40432478054d42deb49876d20376c0f6.jpg',
          },
        ],
      },
    ],
  },
  {
    clipId: 4509,
    media: 1966,
    subtitles: [
      {
        id: 252504,
        lectures: [
          {
            id: '99-6656-9837',
            phrase: 'Don’t think you can get away with this!',
            thumbnailUrl:
              'https://public.realclass.co.kr/video/lecture/6ab1264b30d54557928fe57087de9417/6ab1264b30d54557928fe57087de9417.jpg',
          },
        ],
      },
      {
        id: 252554,
        lectures: [
          {
            id: '99-6656-9838',
            phrase: 'How’d you like to lend a hand with the "Pelicans"?',
            thumbnailUrl:
              'https://public.realclass.co.kr/video/lecture/840e7565cf4e45ecabc0b9a0076e776c/840e7565cf4e45ecabc0b9a0076e776c.jpg',
          },
        ],
      },
    ],
  },
  {
    clipId: 4510,
    media: 1967,
    subtitles: [
      {
        id: 252585,
        lectures: [
          {
            id: '99-6657-9839',
            phrase: 'I’ll fix the lunch',
            thumbnailUrl:
              'https://public.realclass.co.kr/video/lecture/d9a1ea7d785a4d179d69214085c4a144/d9a1ea7d785a4d179d69214085c4a144.jpg',
          },
        ],
      },
      {
        id: 252598,
        lectures: [
          {
            id: '99-6657-9840',
            phrase: 'They’re way too long!',
            thumbnailUrl:
              'https://public.realclass.co.kr/video/lecture/e95af4592958444488d045416386a0df/e95af4592958444488d045416386a0df.jpg',
          },
        ],
      },
    ],
  },
  {
    clipId: 4511,
    media: 1968,
    subtitles: [
      {
        id: 252678,
        lectures: [
          {
            id: '99-6658-9841',
            phrase: 'You’re the last person in the world who should be writing a book',
            thumbnailUrl:
              'https://public.realclass.co.kr/video/lecture/c1953881689443ddb6b31ada0c3b8239/c1953881689443ddb6b31ada0c3b8239.jpg',
          },
        ],
      },
      {
        id: 252703,
        lectures: [
          {
            id: '99-6658-9842',
            phrase: 'What’s it for?',
            thumbnailUrl:
              'https://public.realclass.co.kr/video/lecture/30c3043b26b345a799237e278b8ae3e6/30c3043b26b345a799237e278b8ae3e6.jpg',
          },
        ],
      },
    ],
  },
  {
    clipId: 4512,
    media: 1969,
    subtitles: [
      {
        id: 252756,
        lectures: [
          {
            id: '99-6659-9843',
            phrase: 'Well, since I’m already wet.',
            thumbnailUrl:
              'https://public.realclass.co.kr/video/lecture/5b42c24ec6c345199729b58e1448495b/5b42c24ec6c345199729b58e1448495b.jpg',
          },
        ],
      },
    ],
  },
  {
    clipId: 4513,
    media: 1970,
    subtitles: [
      {
        id: 252818,
        lectures: [
          {
            id: '99-6660-9844',
            phrase: 'If a teacher doesn’t like your looks, there’s nothing you can do.',
            thumbnailUrl:
              'https://public.realclass.co.kr/video/lecture/04ca6970412b4f8aa91b9316e9efd242/04ca6970412b4f8aa91b9316e9efd242.jpg',
          },
        ],
      },
      {
        id: 252865,
        lectures: [
          {
            id: '99-6660-9845',
            phrase: 'Why don’t you go first?',
            thumbnailUrl:
              'https://public.realclass.co.kr/video/lecture/cd77e0f56d70473499d5c2880fe876eb/cd77e0f56d70473499d5c2880fe876eb.jpg',
          },
        ],
      },
    ],
  },
  {
    clipId: 4514,
    media: 1971,
    subtitles: [
      {
        id: 252932,
        lectures: [
          {
            id: '99-6661-9846',
            phrase: 'some of us don’t take any chances.',
            thumbnailUrl:
              'https://public.realclass.co.kr/video/lecture/26cdb93e8b7543489de11ce73a76a1fd/26cdb93e8b7543489de11ce73a76a1fd.jpg',
          },
        ],
      },
      {
        id: 252943,
        lectures: [
          {
            id: '99-6661-9847',
            phrase: 'For some reason, I just can’t picture Santa Claus scuba diving.',
            thumbnailUrl:
              'https://public.realclass.co.kr/video/lecture/055a5e561a0e4885bce6f23a3cde1982/055a5e561a0e4885bce6f23a3cde1982.jpg',
          },
        ],
      },
    ],
  },
  {
    clipId: 4515,
    media: 1972,
    subtitles: [
      {
        id: 253008,
        lectures: [
          {
            id: '99-6662-9848',
            phrase: "It’s worth thinking about, isn't it?",
            thumbnailUrl:
              'https://public.realclass.co.kr/video/lecture/2071be6de9784d2f9a382ddc61d15a33/2071be6de9784d2f9a382ddc61d15a33.jpg',
          },
        ],
      },
      {
        id: 253019,
        lectures: [
          {
            id: '99-6662-9849',
            phrase: 'Peanut butter it is!',
            thumbnailUrl:
              'https://public.realclass.co.kr/video/lecture/51cd600910524d85b09a1a9f1939c0c4/51cd600910524d85b09a1a9f1939c0c4.jpg',
          },
        ],
      },
    ],
  },
  {
    clipId: 4516,
    media: 1973,
    subtitles: [
      {
        id: 253091,
        lectures: [
          {
            id: '99-6663-9850',
            phrase: 'What was that?',
            thumbnailUrl:
              'https://public.realclass.co.kr/video/lecture/c78a9ebe3a5a435583d5923163ac85a8/c78a9ebe3a5a435583d5923163ac85a8.jpg',
          },
        ],
      },
      {
        id: 253135,
        lectures: [
          {
            id: '99-6663-9851',
            phrase: 'Who’s the kid with the blanket?',
            thumbnailUrl:
              'https://public.realclass.co.kr/video/lecture/6603d5e030ee4de9a2a9cd3d14fadd58/6603d5e030ee4de9a2a9cd3d14fadd58.jpg',
          },
        ],
      },
    ],
  },
  {
    clipId: 4517,
    media: 1974,
    subtitles: [
      {
        id: 253189,
        lectures: [
          {
            id: '99-6664-9852',
            phrase: 'Well, that’s more direct, and to the point.',
            thumbnailUrl:
              'https://public.realclass.co.kr/video/lecture/c1f7b00c84aa4516917c8bcd687c0a63/c1f7b00c84aa4516917c8bcd687c0a63.jpg',
          },
        ],
      },
      {
        id: 253222,
        lectures: [
          {
            id: '99-6664-9853',
            phrase: 'If you can’t make things up, why not try a biography?',
            thumbnailUrl:
              'https://public.realclass.co.kr/video/lecture/fed362b8e7be44a1b43e43b298829fe5/fed362b8e7be44a1b43e43b298829fe5.jpg',
          },
        ],
      },
    ],
  },
  {
    clipId: 4518,
    media: 1975,
    subtitles: [
      {
        id: 253277,
        lectures: [
          {
            id: '99-6665-9854',
            phrase: 'I don’t think so, Marcie.',
            thumbnailUrl:
              'https://public.realclass.co.kr/video/lecture/1e7f627590184f898e950e095e984ad8/1e7f627590184f898e950e095e984ad8.jpg',
          },
        ],
      },
      {
        id: 253328,
        lectures: [
          {
            id: '99-6665-9855',
            phrase: 'I’ll be over as soon as I pack.',
            thumbnailUrl:
              'https://public.realclass.co.kr/video/lecture/5eeb333edd904a12aa416dd82a5df548/5eeb333edd904a12aa416dd82a5df548.jpg',
          },
        ],
      },
    ],
  },
  {
    clipId: 4519,
    media: 1976,
    subtitles: [
      {
        id: 253410,
        lectures: [
          {
            id: '99-6666-9856',
            phrase: 'There’s talk going around that I shouldn’t let you dominate me.',
            thumbnailUrl:
              'https://public.realclass.co.kr/video/lecture/7d16edb6c7da4fb5bd158d91c02cc548/7d16edb6c7da4fb5bd158d91c02cc548.jpg',
          },
        ],
      },
    ],
  },
  {
    clipId: 4520,
    media: 1977,
    subtitles: [
      {
        id: 253447,
        lectures: [
          {
            id: '99-6667-9857',
            phrase: 'I’ll have all four books read by the time school starts.',
            thumbnailUrl:
              'https://public.realclass.co.kr/video/lecture/eace001000c44a8ba8e45e5d7c9af095/eace001000c44a8ba8e45e5d7c9af095.jpg',
          },
        ],
      },
      {
        id: 253486,
        lectures: [
          {
            id: '99-6667-9858',
            phrase: 'I’m not used to physical labor!',
            thumbnailUrl:
              'https://public.realclass.co.kr/video/lecture/2d5f92bbe3364ec7b7cf166aa40ae161/2d5f92bbe3364ec7b7cf166aa40ae161.jpg',
          },
        ],
      },
    ],
  },
  {
    clipId: 4521,
    media: 1978,
    subtitles: [
      {
        id: 253510,
        lectures: [
          {
            id: '99-6668-9859',
            phrase: 'I said he’s out of his mind!',
            thumbnailUrl:
              'https://public.realclass.co.kr/video/lecture/840c07005fa440b7b90da2e5d2cabee8/840c07005fa440b7b90da2e5d2cabee8.jpg',
          },
        ],
      },
      {
        id: 253540,
        lectures: [
          {
            id: '99-6668-9860',
            phrase: 'Sir, why do we play football in the rain?',
            thumbnailUrl:
              'https://public.realclass.co.kr/video/lecture/6d695fdb1db74d898e4fa7b43297dff1/6d695fdb1db74d898e4fa7b43297dff1.jpg',
          },
        ],
      },
    ],
  },
  {
    clipId: 4522,
    media: 1979,
    subtitles: [
      {
        id: 253627,
        lectures: [
          {
            id: '99-6669-9861',
            phrase: 'Chuck, I need a favor.',
            thumbnailUrl:
              'https://public.realclass.co.kr/video/lecture/0f6d5d96760847aebc7c629da94a8e8b/0f6d5d96760847aebc7c629da94a8e8b.jpg',
          },
        ],
      },
    ],
  },
  {
    clipId: 4523,
    media: 1980,
    subtitles: [
      {
        id: 253675,
        lectures: [
          {
            id: '99-6670-9862',
            phrase: 'Okay, let’s get a few things straight.',
            thumbnailUrl:
              'https://public.realclass.co.kr/video/lecture/2aa9532479d94713a38eadfe618ae008/2aa9532479d94713a38eadfe618ae008.jpg',
          },
        ],
      },
      {
        id: 253696,
        lectures: [
          {
            id: '99-6670-9863',
            phrase: 'That’s the spirit, partner!',
            thumbnailUrl:
              'https://public.realclass.co.kr/video/lecture/2f539bdb6c974619a63db31ed4bc5ed6/2f539bdb6c974619a63db31ed4bc5ed6.jpg',
          },
        ],
      },
    ],
  },
  {
    clipId: 4524,
    media: 1981,
    subtitles: [
      {
        id: 253797,
        lectures: [
          {
            id: '99-6671-9864',
            phrase: 'I hold a grudge against them.',
            thumbnailUrl:
              'https://public.realclass.co.kr/video/lecture/d8678e950c814b0d95ed9028d363ebe5/d8678e950c814b0d95ed9028d363ebe5.jpg',
          },
        ],
      },
      {
        id: 253803,
        lectures: [
          {
            id: '99-6671-9865',
            phrase: 'Maybe it’ll make her feel bad.',
            thumbnailUrl:
              'https://public.realclass.co.kr/video/lecture/5cff3fe6f6564aaab7efcee88b4e0470/5cff3fe6f6564aaab7efcee88b4e0470.jpg',
          },
        ],
      },
    ],
  },
  {
    clipId: 4525,
    media: 1982,
    subtitles: [
      {
        id: 253861,
        lectures: [
          {
            id: '99-6672-9866',
            phrase: 'Well, that’s very comforting.',
            thumbnailUrl:
              'https://public.realclass.co.kr/video/lecture/d3418742304f4fcb9efd9ba8128a3fea/d3418742304f4fcb9efd9ba8128a3fea.jpg',
          },
        ],
      },
      {
        id: 253905,
        lectures: [
          {
            id: '99-6672-9867',
            phrase: 'You and I going to the movies.',
            thumbnailUrl:
              'https://public.realclass.co.kr/video/lecture/5353f2fe86654ab2ad6e1935a7b514a6/5353f2fe86654ab2ad6e1935a7b514a6.jpg',
          },
        ],
      },
    ],
  },
  {
    clipId: 4526,
    media: 1983,
    subtitles: [
      {
        id: 253961,
        lectures: [
          {
            id: '99-6673-9868',
            phrase: 'it’s hard to bark with a supper dish in your mouth.',
            thumbnailUrl:
              'https://public.realclass.co.kr/video/lecture/1fee75a9485e47a8a4bcf69115778e57/1fee75a9485e47a8a4bcf69115778e57.jpg',
          },
        ],
      },
    ],
  },
  {
    clipId: 4527,
    media: 1984,
    subtitles: [
      {
        id: 254029,
        lectures: [
          {
            id: '99-6674-9869',
            phrase: 'It’ll be here in one second.',
            thumbnailUrl:
              'https://public.realclass.co.kr/video/lecture/f4ffb6452f564bd0b0483c42ded3af9d/f4ffb6452f564bd0b0483c42ded3af9d.jpg',
          },
        ],
      },
    ],
  },
  {
    clipId: 4528,
    media: 1985,
    subtitles: [
      {
        id: 254120,
        lectures: [
          {
            id: '99-6675-9870',
            phrase: 'Stomach’s not too happy, is it?',
            thumbnailUrl:
              'https://public.realclass.co.kr/video/lecture/a49bf74950d54a4c95038ea515b6d074/a49bf74950d54a4c95038ea515b6d074.jpg',
          },
        ],
      },
      {
        id: 254149,
        lectures: [
          {
            id: '99-6675-9871',
            phrase: 'Bon appétit!',
            thumbnailUrl:
              'https://public.realclass.co.kr/video/lecture/98f8ea7bb4104693aaa9cf08975f3ee0/98f8ea7bb4104693aaa9cf08975f3ee0.jpg',
          },
        ],
      },
    ],
  },
  {
    clipId: 4529,
    media: 1986,
    subtitles: [
      {
        id: 254197,
        lectures: [
          {
            id: '99-6676-9872',
            phrase: 'They’ll hog the court all day!',
            thumbnailUrl:
              'https://public.realclass.co.kr/video/lecture/9b14261a67064866911103c95f534ddb/9b14261a67064866911103c95f534ddb.jpg',
          },
        ],
      },
    ],
  },
  {
    clipId: 4530,
    media: 1987,
    subtitles: [
      {
        id: 254276,
        lectures: [
          {
            id: '99-6677-9873',
            phrase: 'Your best buddy Linus got me nothing!',
            thumbnailUrl:
              'https://public.realclass.co.kr/video/lecture/2b53790676fd49849bdeb3eeec9b881b/2b53790676fd49849bdeb3eeec9b881b.jpg',
          },
        ],
      },
    ],
  },
  {
    clipId: 4531,
    media: 1988,
    subtitles: [
      {
        id: 254373,
        lectures: [
          {
            id: '99-6678-9874',
            phrase: 'Get to work!',
            thumbnailUrl:
              'https://public.realclass.co.kr/video/lecture/ae9699566a8742c4a4f543d18d190b0b/ae9699566a8742c4a4f543d18d190b0b.jpg',
          },
        ],
      },
      {
        id: 254388,
        lectures: [
          {
            id: '99-6678-9875',
            phrase: 'why’d you have to bring up "the Great Pumpkin"?',
            thumbnailUrl:
              'https://public.realclass.co.kr/video/lecture/37a2c549ab0149aeb51171d779ab66cb/37a2c549ab0149aeb51171d779ab66cb.jpg',
          },
        ],
      },
    ],
  },
  {
    clipId: 4532,
    media: 1989,
    subtitles: [
      {
        id: 254435,
        lectures: [
          {
            id: '99-6679-9876',
            phrase: 'Your sister’s losing it.',
            thumbnailUrl:
              'https://public.realclass.co.kr/video/lecture/e00bdbae19c74d109e238c3f1d221bba/e00bdbae19c74d109e238c3f1d221bba.jpg',
          },
        ],
      },
      {
        id: 254440,
        lectures: [
          {
            id: '99-6679-9877',
            phrase: 'What do you mean, not for you?',
            thumbnailUrl:
              'https://public.realclass.co.kr/video/lecture/0202185a57e54976b3e4b1d42b453987/0202185a57e54976b3e4b1d42b453987.jpg',
          },
        ],
      },
    ],
  },
  {
    clipId: 4533,
    media: 1990,
    subtitles: [
      {
        id: 254514,
        lectures: [
          {
            id: '99-6680-9878',
            phrase: 'That’s better!',
            thumbnailUrl:
              'https://public.realclass.co.kr/video/lecture/8f92f777702f4458a51c24e6190177b9/8f92f777702f4458a51c24e6190177b9.jpg',
          },
        ],
      },
      {
        id: 254578,
        lectures: [
          {
            id: '99-6680-9879',
            phrase: 'it’s kind of hard to say.',
            thumbnailUrl:
              'https://public.realclass.co.kr/video/lecture/ffb65f148963498593cbd20ffea4c3cc/ffb65f148963498593cbd20ffea4c3cc.jpg',
          },
        ],
      },
    ],
  },
  {
    clipId: 4534,
    media: 1991,
    subtitles: [
      {
        id: 254644,
        lectures: [
          {
            id: '99-6681-9880',
            phrase: 'I knew I had the wrong thumb.',
            thumbnailUrl:
              'https://public.realclass.co.kr/video/lecture/bae4002251064b70bd967b309aec5db1/bae4002251064b70bd967b309aec5db1.jpg',
          },
        ],
      },
    ],
  },
  {
    clipId: 4535,
    media: 1992,
    subtitles: [
      {
        id: 254710,
        lectures: [
          {
            id: '99-6682-9881',
            phrase: 'That should count for something, shouldn’t it?',
            thumbnailUrl:
              'https://public.realclass.co.kr/video/lecture/960e36846dc044c797dedb86c92602a5/960e36846dc044c797dedb86c92602a5.jpg',
          },
        ],
      },
      {
        id: 254734,
        lectures: [
          {
            id: '99-6682-9882',
            phrase: 'It can’t hurt to ask.',
            thumbnailUrl:
              'https://public.realclass.co.kr/video/lecture/61f736a249ed4459a9f9e1aefa42dcf0/61f736a249ed4459a9f9e1aefa42dcf0.jpg',
          },
        ],
      },
    ],
  },
  {
    clipId: 4536,
    media: 1993,
    subtitles: [
      {
        id: 254809,
        lectures: [
          {
            id: '99-6683-9883',
            phrase: 'So long, old friend!',
            thumbnailUrl:
              'https://public.realclass.co.kr/video/lecture/f82008772d334dd2be6831a89534a811/f82008772d334dd2be6831a89534a811.jpg',
          },
        ],
      },
      {
        id: 254839,
        lectures: [
          {
            id: '99-6683-9884',
            phrase: 'He looks anything but edible.',
            thumbnailUrl:
              'https://public.realclass.co.kr/video/lecture/a3699ceafcc74a74b39db1952546c1a1/a3699ceafcc74a74b39db1952546c1a1.jpg',
          },
        ],
      },
    ],
  },
  {
    clipId: 4537,
    media: 1994,
    subtitles: [
      {
        id: 254874,
        lectures: [
          {
            id: '99-6684-9885',
            phrase: 'Suit yourself.',
            thumbnailUrl:
              'https://public.realclass.co.kr/video/lecture/275d4042f65e41e19f53dabf332902e8/275d4042f65e41e19f53dabf332902e8.jpg',
          },
        ],
      },
      {
        id: 254912,
        lectures: [
          {
            id: '99-6684-9886',
            phrase: 'I’m glad you didn’t hear that.',
            thumbnailUrl:
              'https://public.realclass.co.kr/video/lecture/fe110aa4c7314fe89e5dcd1ff0171f64/fe110aa4c7314fe89e5dcd1ff0171f64.jpg',
          },
        ],
      },
    ],
  },
  {
    clipId: 4538,
    media: 1995,
    subtitles: [
      {
        id: 254946,
        lectures: [
          {
            id: '99-6685-9887',
            phrase: 'You’re afraid to do something about it!',
            thumbnailUrl:
              'https://public.realclass.co.kr/video/lecture/0705ea7a111444cf9e071f08230d1b41/0705ea7a111444cf9e071f08230d1b41.jpg',
          },
        ],
      },
      {
        id: 255005,
        lectures: [
          {
            id: '99-6685-9888',
            phrase: 'What do you think?',
            thumbnailUrl:
              'https://public.realclass.co.kr/video/lecture/e29d68f4a7fb42e7a76dcce2f166803a/e29d68f4a7fb42e7a76dcce2f166803a.jpg',
          },
        ],
      },
    ],
  },
  {
    clipId: 4539,
    media: 1996,
    subtitles: [
      {
        id: 255079,
        lectures: [
          {
            id: '99-6686-9889',
            phrase: 'it’s killing me!',
            thumbnailUrl:
              'https://public.realclass.co.kr/video/lecture/5b97d5039d7644fdac082a6c5c4bee77/5b97d5039d7644fdac082a6c5c4bee77.jpg',
          },
        ],
      },
      {
        id: 255090,
        lectures: [
          {
            id: '99-6686-9890',
            phrase: 'This is it, partner!',
            thumbnailUrl:
              'https://public.realclass.co.kr/video/lecture/f0f54d5b09fa4de0b2730df7050765d7/f0f54d5b09fa4de0b2730df7050765d7.jpg',
          },
        ],
      },
    ],
  },
]
