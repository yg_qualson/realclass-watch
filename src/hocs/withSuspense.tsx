import React from 'react'

export default function withSuspense<P>(Component: React.ComponentType<P>, Loading: React.ComponentType) {
  return (props: P) => {
    return React.createElement(
      React.Suspense,
      { fallback: React.createElement(Loading, null) },
      React.createElement(Component, props)
    )
  }
}
