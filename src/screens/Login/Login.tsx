import { REALCLASS_URL } from '@env'
import React from 'react'
import WebView from 'react-native-webview'

import useBridge from '@/hooks/useBridge'

const clearTokens = `(function(){ window.localStorage.clear() })();`

function Login() {
  const messageHandler = useBridge()

  return (
    <WebView
      source={{ uri: `${REALCLASS_URL}/account/login` }}
      onMessage={messageHandler}
      injectedJavaScript={clearTokens}
    />
  )
}

export default Login
