import React from 'react'
import { SafeAreaView, Text, TouchableOpacity, View } from 'react-native'
import { useRecoilValue } from 'recoil'

import withSuspense from '@/hocs/withSuspense'
import { mySeasons } from '@/modules/media/atoms'
import { SeasonListScreenProps } from '@/navigation/UserNavigation.types'

import * as S from './SeasonList.style'

const SeasonList = ({ navigation }: SeasonListScreenProps) => {
  const seasons = useRecoilValue(mySeasons)
  function onPress(seasonId: number) {
    navigation.push('MediaList', { seasonId })
  }
  return (
    <S.List bounces={false}>
      <SafeAreaView>
        {seasons.map(({ season }) => (
          <TouchableOpacity key={season.id} onPress={() => onPress(season.id)}>
            <View>
              <S.Thumbnail source={{ uri: season.thumbnailUrl }} />
              <S.Title>{season.seasonTitle}</S.Title>
            </View>
          </TouchableOpacity>
        ))}
      </SafeAreaView>
    </S.List>
  )
}

export default withSuspense(SeasonList, () => <Text>Loading</Text>)
