import React from 'react'
import { SafeAreaView, Text, TouchableOpacity, View } from 'react-native'
import { useRecoilValue } from 'recoil'

import withSuspense from '@/hocs/withSuspense'
import { mediaList, mySeasons } from '@/modules/media/atoms'
import { MediaListScreenProps } from '@/navigation/UserNavigation.types'

import * as S from './MediaList.style'

const MediaList = ({ navigation, route }: MediaListScreenProps) => {
  const seasonId = route.params.seasonId
  const courseId = useRecoilValue(mySeasons).find(({ season }) => season.id === seasonId)?.courseIds[0] || 0
  const medias = useRecoilValue(mediaList(courseId))

  function onPress(mediaId: number) {
    navigation.push('Watch', { seasonId, mediaId })
  }

  return (
    <S.List bounces={false}>
      <SafeAreaView>
        {medias.map(({ id, name, thumbnailUrl }) => (
          <TouchableOpacity key={id} onPress={() => onPress(id)}>
            <S.Thumbnail source={{ uri: thumbnailUrl }} />
            <S.Title>{name}</S.Title>
          </TouchableOpacity>
        ))}
        <Text>{seasonId}</Text>
      </SafeAreaView>
    </S.List>
  )
}

export default withSuspense(MediaList, () => <Text>Loading</Text>)
