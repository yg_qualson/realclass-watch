import styled from '@emotion/native'

export const List = styled.ScrollView({
  paddingTop: 20,
  paddingBottom: 20,
})
export const Thumbnail = styled.Image({
  width: '90%',
  height: 200,
  alignSelf: 'center',
  borderRadius: 10,
})
export const Title = styled.Text({
  fontSize: 20,
  textAlign: 'center',
  marginBottom: 10,
})
