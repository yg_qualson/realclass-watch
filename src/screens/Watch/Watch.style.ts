import styled from '@emotion/native'
import { SafeAreaView } from 'react-native'
import NativeVideo from 'react-native-video'

export const ForDevs = styled.Text({
  position: 'absolute',
  top: 100,
  textAlign: 'center',
  color: 'black',
  fontSize: 20,
})

export const Video = styled(NativeVideo)({
  width: '100%',
  height: '100%',
})

export const Overlay = styled(SafeAreaView)({
  position: 'absolute',
  width: '100%',
  height: '100%',
})

export const OverlayContent = styled.View({
  position: 'relative',
  width: '100%',
  height: '100%',
})

export const Exit = styled.Pressable({
  width: 50,
  padding: 10,
  backgroundColor: '#fff',
  color: '#000',
})

export const ExitText = styled.Text({
  fontSize: 20,
})

export const SubtitleContainer = styled.View({
  position: 'absolute',
  bottom: 0,
  width: '100%',
  alignItems: 'center',
})

export const Subtitle = styled.View({
  padding: 4,
  backgroundColor: 'rgba(0, 0, 0, 0.5)',
})

export const SubtitleText = styled.Text({
  color: '#fff',
  textAlign: 'center',
})
