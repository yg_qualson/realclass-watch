import { Platform } from 'react-native'

const Watch = Platform.select({
  ios: () => require('./Watch.ios'),
  android: () => require('./Watch.android'),
  default: () => require('./Watch.ios'),
})()

export default Watch.default
