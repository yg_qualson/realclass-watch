import axios from 'axios'
import React, { useEffect, useRef } from 'react'
import { Text, Platform } from 'react-native'
import Orientation from 'react-native-orientation'
import Video, { DRMType } from 'react-native-video'
import { useRecoilValue } from 'recoil'

import withSuspense from '@/hocs/withSuspense'
import { ACCESS_TOKEN } from '@/modules/auth/atoms'
import { media } from '@/modules/media/atoms'
import { Subtitle } from '@/modules/media/models'
import { loadValue } from '@/modules/storage'
import { WatchScreenProps } from '@/navigation/UserNavigation.types'

import * as S from './Watch.style'

/**
 * THIS CODE DOES NOT WORK IN iOS Simulator
 * cause DRM issue
 */
const Watch = ({ navigation, route }: WatchScreenProps) => {
  const { seasonId, mediaId } = route.params
  const mediaInfo = useRecoilValue(media({ seasonId, mediaId }))
  const video = useRef<Video>(null)
  const [paused, setPaused] = React.useState(false)
  const [currentTime, setCurrentTime] = React.useState(mediaInfo.startTime)
  const [currentSubtitle, setCurrentSubtitle] = React.useState<Subtitle>()

  useEffect(() => {
    return () => setPaused(true)
  }, [])

  useEffect(() => {
    const subtitle = mediaInfo.subtitles.find(
      ({ startTime, endTime }) => startTime <= currentTime && currentTime <= endTime
    )
    setCurrentSubtitle(subtitle)
  }, [mediaInfo, currentTime])

  function goFullscreen() {
    Orientation.lockToLandscape()
    navigation.setOptions({ headerShown: false })
    setPaused(false)
    video.current?.seek(32.5)
  }

  function goBack() {
    Orientation.lockToPortrait()
    navigation.setOptions({ headerShown: true })
    setPaused(true)
    navigation.pop()
  }

  return (
    <>
      <S.ForDevs>DRM Video can not be played in simulator</S.ForDevs>
      <S.Video
        ref={video}
        paused={paused}
        source={{ uri: mediaInfo.videoUrlFairplay }}
        drm={{
          type: DRMType.FAIRPLAY,
          certificateUrl: 'https://public.realclass.co.kr/video/fairplay.cer',
          getLicense: getLicense(mediaId),
        }}
        onLoad={goFullscreen}
        ignoreSilentSwitch='ignore'
        progressUpdateInterval={100}
        onProgress={({ currentTime }) => setCurrentTime(currentTime)}
      />
      <S.Overlay>
        <S.OverlayContent>
          <S.Exit onPress={goBack}>
            <S.ExitText>X</S.ExitText>
          </S.Exit>
          <CurrentSubtitle subtitle={currentSubtitle} />
        </S.OverlayContent>
      </S.Overlay>
    </>
  )
}

export default withSuspense(Watch, () => <Text>Loading</Text>)

const getLicense = (mediaId: number) => async (spcString: string) => {
  const accessToken = await loadValue(ACCESS_TOKEN)
  const tokens = await axios
    .get(`https://api-v2-dev.realclass.co.kr/v2/api/profiles/me/drm?media_type=MEDIA&id=${mediaId}`, {
      method: 'GET',
      headers: {
        Authorization: `Bearer ${accessToken}`,
      },
      withCredentials: true,
    })
    .then((response) => response.data)
  const drmToken = tokens[Platform.OS === 'ios' ? DRMType.FAIRPLAY : DRMType.WIDEVINE]

  const formData = new FormData()
  formData.append('spc', spcString)
  const licenseToken = await axios
    .post<string>(`https://license.pallycon.com/ri/licenseManager.do`, formData, {
      headers: { 'pallycon-customdata-v2': drmToken },
    })
    .then((response) => response.data)
  return licenseToken
}

interface SubtitleProps {
  subtitle: Subtitle | undefined
}
const CurrentSubtitle = ({ subtitle }: SubtitleProps) => {
  return (
    <S.SubtitleContainer>
      {subtitle && (
        <S.Subtitle>
          <S.SubtitleText>{subtitle.textEng}</S.SubtitleText>
        </S.Subtitle>
      )}
    </S.SubtitleContainer>
  )
}
