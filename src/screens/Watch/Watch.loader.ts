import axios from 'axios'
import React from 'react'
import { Platform } from 'react-native'
import { DRMType } from 'react-native-video'
import { useRecoilValue } from 'recoil'

import { accessToken } from '@/modules/auth/atoms'

export const DRM_FOR_LECTURE_URL = 'https://api-v2.realclass.co.kr/v2/api/profiles/me/drm?media_type=LECTURE&id=7'

export const DRM_LECTURE_URL = {
  ios: 'https://public.realclass.co.kr/video/lecture/4154283780e14afbaceaa0b0ed33ce9a/drm/hls/4154283780e14afbaceaa0b0ed33ce9a.m3u8',
  android:
    'https://public.realclass.co.kr/video/lecture/4154283780e14afbaceaa0b0ed33ce9a/drm/dash/4154283780e14afbaceaa0b0ed33ce9a.mpd',
}

export const NON_DRM_VIDEO = 'https://public.realclass.co.kr/video/interview/1/tyler_beginner_720p.mp4'

export function useDRMToken(url: string) {
  const ACCESS_TOKEN = useRecoilValue(accessToken)
  const [token, setToken] = React.useState('')

  React.useEffect(() => {
    axios
      .get(url, {
        method: 'GET',
        headers: {
          Authorization: `Bearer ${ACCESS_TOKEN}`,
        },
        withCredentials: true,
      })
      .then((response) => response.data)
      .then((data) => setToken(data[Platform.OS === 'ios' ? DRMType.FAIRPLAY : DRMType.WIDEVINE]))
      .catch((error) => {
        console.warn(JSON.stringify(error.response, null, 2))
      })
  }, [url, ACCESS_TOKEN])

  return token
}
