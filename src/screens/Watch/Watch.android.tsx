import React from 'react'
import { StyleSheet } from 'react-native'
import { SafeAreaView } from 'react-native-safe-area-context'
import Video, { DRMType } from 'react-native-video'

import { DRM_FOR_LECTURE_URL, DRM_LECTURE_URL, useDRMToken } from './Watch.loader'

const Watch: React.FC = () => {
  const token = useDRMToken(DRM_FOR_LECTURE_URL)

  if (!token) return null

  return (
    <SafeAreaView>
      <Video
        source={{ uri: DRM_LECTURE_URL.android }}
        drm={{
          type: DRMType.WIDEVINE,
          licenseServer: 'https://license.pallycon.com/ri/licenseManager.do',
          headers: { 'pallycon-customdata-v2': token },
        }}
        controls
        style={styles.backgroundVideo}
      />
    </SafeAreaView>
  )
}

const styles = StyleSheet.create({
  backgroundVideo: {
    width: '100%',
    height: 280,
    backgroundColor: 'black',
  },
})

export default Watch
