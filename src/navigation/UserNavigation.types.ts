import { NavigatorScreenParams } from '@react-navigation/core'
import { StackScreenProps } from '@react-navigation/stack'

export type ParamListOf<Params, K extends keyof Params> = Params[K] extends NavigatorScreenParams<infer P>
  ? P
  : Params[K]

export type UserNavigationParamList = {
  SeasonList: undefined
  MediaList: { seasonId: number }
  Watch: { seasonId: number; mediaId: number }
}

export type SeasonListScreenProps = StackScreenProps<UserNavigationParamList, 'SeasonList'>
export type MediaListScreenProps = StackScreenProps<UserNavigationParamList, 'MediaList'>
export type WatchScreenProps = StackScreenProps<UserNavigationParamList, 'Watch'>
