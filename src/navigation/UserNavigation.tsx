import { createStackNavigator } from '@react-navigation/stack'
import React from 'react'

import MediaList from '@/screens/MediaList'
import SeasonList from '@/screens/SeasonList'
import Watch from '@/screens/Watch'

const Stack = createStackNavigator()

const UserNavigation = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen name='SeasonList' component={SeasonList} options={{ headerTitle: 'Realclass:Watch' }} />
      <Stack.Screen name='MediaList' component={MediaList} />
      <Stack.Screen name='Watch' component={Watch} />
    </Stack.Navigator>
  )
}

export default UserNavigation
