import React from 'react'

import Login from '@/screens/Login'

const GuestNavigation = () => {
  return <Login />
}

export default GuestNavigation
