module.exports = {
  presets: ['module:metro-react-native-babel-preset'],
  plugins: ['babel-plugin-tsconfig-paths', 'module:react-native-dotenv'],
}
